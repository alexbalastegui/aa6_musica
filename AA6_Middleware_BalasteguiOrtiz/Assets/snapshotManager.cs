﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class snapshotManager : MonoBehaviour {


    [FMODUnity.EventRef]
    public string defaultSnapshot;
    
    public FMOD.Studio.EventInstance snapshotInstance;

    // Use this for initialization
    void Start () {
        snapshotInstance = FMODUnity.RuntimeManager.CreateInstance(defaultSnapshot);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(snapshotInstance, transform, GetComponent<Rigidbody>());
        snapshotInstance.start();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
}
