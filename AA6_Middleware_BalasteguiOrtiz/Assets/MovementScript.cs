﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

    Rigidbody rb;
    public float turnRatio = 1f;
    public float velocity = 5f;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
        {
            rb.velocity = new Vector3(transform.forward.x*velocity, rb.velocity.y, transform.forward.z*velocity);
        }
        else if(Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector3(transform.forward.x*(-velocity), rb.velocity.y, transform.forward.z*(-velocity));
        }
        else if(Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector3(transform.right.x * (-velocity), rb.velocity.y, transform.right.z * (-velocity));
        }
        else if(Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector3(transform.right.x* velocity, rb.velocity.y, transform.right.z* velocity);
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }

        if (Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(0, -turnRatio*Time.deltaTime, 0);
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            transform.Rotate(0, turnRatio*Time.deltaTime, 0);
        }

    }
}
