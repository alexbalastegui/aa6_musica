﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraBehavioure : MonoBehaviour {
    public float turnRatio = 15f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetAxis("Mouse X") < 0)
        {
            transform.Rotate(0, -turnRatio * Time.deltaTime, 0);
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            transform.Rotate(0, turnRatio * Time.deltaTime, 0);
        }
    }
}
