﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgMusicManager : MonoBehaviour {
    
    [FMODUnity.EventRef]
    public string bgMusic;
    
    public FMOD.Studio.EventInstance bgMusicEventInstance;
    public FMOD.Studio.ParameterInstance route;
    private float routeValue;


    // Use this for initialization
    void Start () {

        bgMusicEventInstance = FMODUnity.RuntimeManager.CreateInstance(bgMusic);
        bgMusicEventInstance.getParameter("Route", out route);
        //bgMusicEventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    private void Update()
    {
        float aux;
        route.getValue(out aux);
        Debug.Log("param value: " + aux);
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("outside"))
    //    {
    //        bgMusicEventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    //        routeValue = 1f;
    //        FMOD.RESULT res = route.setValue(routeValue);
    //        bgMusicEventInstance.start();
    //        Debug.Log(res);
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.CompareTag("outside"))
    //    {
    //        bgMusicEventInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    //        routeValue = 0f;
    //        FMOD.RESULT res = route.setValue(routeValue);
    //        bgMusicEventInstance.start();
    //        Debug.Log(res);
    //    }
    //}
}
