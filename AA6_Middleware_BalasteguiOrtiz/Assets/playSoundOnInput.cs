﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMOD;

public class playSoundOnInput : MonoBehaviour {

    bool shouldPlay = false;
    FMOD.Studio.EventInstance sound;

	// Use this for initialization
	void Start () {
        sound = FMODUnity.RuntimeManager.CreateInstance("event:/waterWellInteraction");
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.E) && shouldPlay)
        {
            sound.start();
            UnityEngine.Debug.Log("rock throw");
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        shouldPlay = true;

    }

    private void OnTriggerExit(Collider other)
    {
        shouldPlay = false;
    }
}
