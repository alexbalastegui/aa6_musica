﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stepSound : MonoBehaviour {

    [FMODUnity.EventRef]
    public string footstepsSound;

    public FMOD.Studio.EventInstance footstepsEvent;
    public FMOD.Studio.ParameterInstance surface;
    private float surfaceValue;

    private void Start()
    {
        footstepsEvent = FMODUnity.RuntimeManager.CreateInstance(footstepsSound);
        footstepsEvent.getParameter("surface", out surface);
    }

    private void Update()
    {
        surface.setValue(surfaceValue);
    }

    public void play()
    {
        footstepsEvent.start();
        float temp;
        surface.getValue(out temp);
    }

    

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.CompareTag("rock"))
        {
            surfaceValue = 0;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("rock"))
        {
            surfaceValue = 1;
        }
    }
    
}
